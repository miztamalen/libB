from conans import ConanFile, CMake, AutoToolsBuildEnvironment, tools
import os

class LibBConan(ConanFile):
    name = "LibB"
    version = "1.0.0"
    license = "<Put the package license here>"
    author = "<Put your name here> <And your email here>"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of Myconan here>"
    topics = ("<Put some tag here>", "<here>", "<and here>")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    requires = "LibA/1.0.0@user/testing"
    #generators = "cmake"
    #exports_sources = "src/*"

    #def build(self):
        #cmake = CMake(self)
        #cmake.configure(source_folder="src")
        #cmake.build()

        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def build(self):
        with tools.chdir("../mak"):
            env_build = AutoToolsBuildEnvironment(self, win_bash=False)
            self.run("make TYPE=staticlib ARCH=x64", win_bash=False)

    def package(self):
        self.copy("*.h", dst="include", src="src")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)
        self.copy("*.exe", dst="bin", keep_path=False)
        self.copy("*.o", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["b"]


	#self.cpp_info.name = "<MYCONAN>"
	#self.cpp_info.names["cmake"] = "<MYCONAN>"
	#self.cpp_info.includedirs = ['include']  # Ordered list of include paths
	#self.cpp_info.libs = []  # The libs to link against
	#self.cpp_info.libdirs = ['lib']  # Directories where libraries can be found
	#self.cpp_info.resdirs = ['res']  # Directories where resources, data, etc can be found
	#self.cpp_info.bindirs = ['bin']  # Directories where executables and shared libs can be found
	#self.cpp_info.srcdirs = []  # Directories where sources can be found (debugging, reusing sources)
	#self.cpp_info.build_modules = []  # Build system utility module files
	#self.cpp_info.defines = []  # preprocessor definitions
	#self.cpp_info.cflags = []  # pure C flags
	#self.cpp_info.cxxflags = []  # C++ compilation flags
	#self.cpp_info.sharedlinkflags = []  # linker flags
	#self.cpp_info.exelinkflags = []  # linker flags
	#self.cpp_info.system_libs = []  # The system libs to link against
